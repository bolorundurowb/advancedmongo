﻿using System.Linq;
using AdvancedMongo.Models.Data;
using Bogus;
using MongoDB.Driver;

namespace AdvancedMongo.DataAccess
{
    public static class DbContextExtensions
    {
        public static void Seed(this DbContext context)
        {
            var ordersExist = context.Orders
                .AsQueryable()
                .Any();

            if (ordersExist)
            {
                return;
            }

            var orderId = 0;
            var orders = new Faker<Order>()
                .RuleFor(x => x.Id, f => orderId++)
                .RuleFor(x => x.Amount, f => f.Random.Number(100, 1000000))
                .RuleFor(x => x.OrderDate, f => f.Date.Past())
                .RuleFor(x => x.Status, f => f.Random.Char('A', 'Z').ToString())
                .Generate(100);
            context.Orders.InsertMany(orders);
        }
    }
}