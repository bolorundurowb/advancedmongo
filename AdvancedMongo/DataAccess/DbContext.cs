﻿using AdvancedMongo.Configuration;
using AdvancedMongo.Models.Data;
using MongoDB.Driver;

namespace AdvancedMongo.DataAccess
{
    public class DbContext
    {
        public IMongoCollection<Order> Orders { get; }

        public DbContext()
        {
            var client = new MongoClient(Config.DbUrl);
            var database = client.GetDatabase(Config.DbName);
            Orders = database.GetCollection<Order>("orders");
        }
    }
}