using System.IO;
using System.Text;
using AdvancedMongo.DataAccess;
using AdvancedMongo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using dotenv.net.DependencyInjection.Microsoft;

namespace AdvancedMongo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddEnv(builder =>
            {
                builder
                    .AddEncoding(Encoding.UTF8)
                    .AddEnvFile(Path.GetFullPath(".env"))
                    .AddThrowOnError(true);
            });

            services.AddScoped<IOrderService, OrderService>();
            services.AddSingleton<DbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            // seed data
            context.Seed();
        }
    }
}