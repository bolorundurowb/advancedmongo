﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdvancedMongo.DataAccess;
using AdvancedMongo.Models.Data;
using AdvancedMongo.Models.DTOs;
using moment.net;
using moment.net.Enums;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace AdvancedMongo.Services
{
    public class OrderService : IOrderService
    {
        private readonly DbContext _dbContext;

        public OrderService(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<OrderSummaryDto>> GetSummaryForDay(DateTime date)
        {
            var start = date.StartOf(DateTimeAnchor.Day);
            var end = date.EndOf(DateTimeAnchor.Day);

            return _dbContext.Orders
                .Aggregate()
                .Match(x => x.OrderDate >= start && x.OrderDate <= end)
                .Group(x => x.Status, g => new OrderSummaryDto
                {
                    Status = g.Key,
                    TotalAmount = g.Sum(z => z.Amount)
                })
                .ToListAsync();
        }

        public Task<Order> Get(int orderId)
        {
            return _dbContext.Orders
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Id == orderId);
        }

        public async Task<Order> Create(string status, int amount)
        {
            var lastOrder = await _dbContext.Orders
                .AsQueryable()
                .OrderByDescending(x => x.Id)
                .FirstOrDefaultAsync();

            var orderId = 1;
            if (lastOrder != null)
            {
                orderId = lastOrder.Id + 1;
            }
            
            var order = new Order(orderId, status, amount);
            await _dbContext.Orders.InsertOneAsync(order);

            return order;
        }

        public async Task<Order> Update(int orderId, string status, int amount)
        {
            var order = await Get(orderId);

            order.UpdateStatus(status);
            order.UpdateAmount(amount);

            await _dbContext.Orders.ReplaceOneAsync(x => x.Id == orderId, order);

            return order;
        }

        public Task Delete(int orderId)
        {
            return _dbContext.Orders.DeleteOneAsync(x => x.Id == orderId);
        }
    }
}