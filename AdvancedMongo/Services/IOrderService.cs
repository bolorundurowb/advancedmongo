﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdvancedMongo.Models.Data;
using AdvancedMongo.Models.DTOs;

namespace AdvancedMongo.Services
{
    public interface IOrderService
    {
        Task<List<OrderSummaryDto>> GetSummaryForDay(DateTime date);

        Task<Order> Get(int orderId);

        Task<Order> Create(string status, int amount);

        Task<Order> Update(int orderId, string status, int amount);

        Task Delete(int orderId);
    }
}