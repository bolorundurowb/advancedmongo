﻿using System;

namespace AdvancedMongo.Configuration
{
    public static class Config
    {
        public static string DbUrl => Environment.GetEnvironmentVariable("MONGO_URL");
        
        public static string DbName => "advanced-mongo";
    }
}