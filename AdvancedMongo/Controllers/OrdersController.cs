﻿using System;
using System.Threading.Tasks;
using AdvancedMongo.Models.Binding;
using AdvancedMongo.Services;
using Microsoft.AspNetCore.Mvc;

namespace AdvancedMongo.Controllers
{
    [ApiController]
    [Route("api/orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet("summary")]
        public async Task<IActionResult> GetDateSummary(DateTime date)
        {
            var summary = await _orderService.GetSummaryForDay(date);
            return Ok(summary);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetOne(int id)
        {
            var order = await _orderService.Get(id);
            return Ok(order);
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] OrderBindingModel bm)
        {
            var order = await _orderService.Create(bm.Status, bm.Amount);
            return Created(order.Id.ToString(), order);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update(int id, [FromBody] OrderBindingModel bm)
        {
            var order = await _orderService.Update(id, bm.Status, bm.Amount);
            return Ok(order);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _orderService.Delete(id);
            return Ok();
        }
    }
}