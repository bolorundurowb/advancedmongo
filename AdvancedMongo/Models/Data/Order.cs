﻿using System;

namespace AdvancedMongo.Models.Data
{
    public class Order
    {
        public int Id { get; private set; }

        public DateTime OrderDate { get; private set; }

        public string Status { get; private set; }

        public int Amount { get; private set; }

        public Order()
        {
        }

        public Order(int id, string status, int amount)
        {
            Id = id;
            OrderDate = DateTime.UtcNow;
            Status = status?.ToUpper();
            Amount = Math.Abs(amount);
        }

        public void UpdateStatus(string status)
        {
            Status = status;
        }

        public void UpdateAmount(int amount)
        {
            Amount = amount;
        }
    }
}