﻿namespace AdvancedMongo.Models.Binding
{
    public class OrderBindingModel
    {
        public string Status { get; set; }

        public int Amount { get; set; }
    }
}