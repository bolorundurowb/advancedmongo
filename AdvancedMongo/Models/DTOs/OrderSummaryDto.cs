﻿namespace AdvancedMongo.Models.DTOs
{
    public class OrderSummaryDto
    {
        public string Status { get; set; }

        public int TotalAmount { get; set; }
    }
}